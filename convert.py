#!/usr/bin/env python3
import argparse
import sys
from os import path
from pathlib import Path
from subprocess import check_call
from shlex import quote
from helper.mysql import MySQL
from helper.setup_logging import configure_logging, RUN_ID
from common.types import *

class Migrator(object):
    # connect via login path (stored in ~/.mylogin.cnf)
    def __init__(self, logger: object, login_path: str = 'client', backup_dir: str = 'backups', dryrun: bool = False, not_before: str = None):
        self.logger = logger
        self.login_path = login_path
        self.backup_dir = backup_dir
        self.dryrun = dryrun
        self.mysql = MySQL(login_path, not_before)

    def create_db_dump(self, dbname: str):
        if not self.dryrun:
            Path(self.backup_dir).mkdir(parents=True, exist_ok=True)
        mysqldump_file = path.join(self.backup_dir, '{}.sql.gz'.format(dbname))
        cmd = "mysqldump --login-path={} --routines --events --single-transaction --databases {} | gzip > {}".format(
            quote(self.login_path), quote(dbname), quote(mysqldump_file)
        )
        self.logger.info(cmd)
        if not self.dryrun:
            check_call(cmd, shell=True)
    
    def create_tbl_dump(self, db: str, tbl: str, structure_only: bool = False):
        dump_dir = path.join(self.backup_dir, 'tbl_struct_dumps' if structure_only else 'tbl_full_dumps')
        mysqldump_file = path.join(dump_dir, '{}.{}.sql'.format(db, tbl))
        if not self.dryrun:
            Path(dump_dir).mkdir(parents=True, exist_ok=True)
        cmd = "mysqldump --login-path={}{} {} {} > {}".format(
            quote(self.login_path), 
            ' --no-data' if structure_only else '', 
            quote(db), 
            quote(tbl), 
            quote(mysqldump_file)
        )
        self.logger.debug(cmd)
        if not self.dryrun:
            check_call(cmd, shell=True)
    
    def restore_tbl_dump(self, db: str, tbl: str):
        dump_dir = path.join(self.backup_dir, 'tbl_full_dumps')
        mysqldump_file = path.join(dump_dir, '{}.{}.sql'.format(db, tbl))
        if not path.isfile(mysqldump_file):
            raise FileNotFoundError(mysqldump_file)
        self.logger.warning('RESTORING table {}.{} from {}'.format(db, tbl, mysqldump_file))
        # the dump contains `DROP TABLE IF EXISTS` so we are good to go
        cmd = "mysql --login-path={} {} < {}".format(quote(self.login_path), quote(db), mysqldump_file)
        self.logger.warning(cmd)
        if not self.dryrun:
            try:
                check_call(cmd, shell=True)
            except CalledProcessError as e:
                msg = 'Could not restore {}: {}'.format(mysqldump_file, e.message)
                self.logger.error(msg)
                raise RuntimeError(msg) # bail out here!
    
    def convert_all(self, db_pattern: str = None, skip_dbs: list = [], limit: int = 0):
        db_count = 0
        # loop over databases that contain at least one MyISAM table
        for db, in self.mysql.get_dbs_with_myisam_tbls(db_pattern):
            if limit > 0 and db_count >= limit:
                break
            if db in skip_dbs:
                self.logger.debug("Skipping database `{}` as it was found in --skip-dbs".format(db))
                continue
            db_count += 1
            # create a full database backup dump
            self.create_db_dump(db)
            # loop over MyISAM tables of that database
            for tbl, row_format, size_kb in self.mysql.get_myisam_tables(db):
                # create backup dump of single table
                self.create_tbl_dump(db, tbl)
                # convert table from MyISAM to InnoDB, NOW!
                alter_stmt = "ALTER TABLE `{}`.`{}` ENGINE=InnoDB".format(db, tbl)
                if row_format == 'Fixed':
                    # workaround for ROW_FORMAT=Fixed which is not supported in InnoDB
                    alter_stmt += ' ROW_FORMAT=DYNAMIC'
                self.logger.info(alter_stmt + '; -- size: {} KiB'.format(size_kb))
                if not self.dryrun:
                    self.mysql.execute(alter_stmt)
                # finally, do a table structure (--no-data) dump check and restore previous backup if failed
                try:
                    self.create_tbl_dump(db, tbl, True)
                except CalledProcessError as e:
                    self.logger.warning('Table structure dump check failed, trying to restore {}.{}'.format(
                        db, tbl
                    ))
                    self.restore_tbl_dump(db, tbl)


if __name__ == "__main__":
    # argument parsing
    parser = argparse.ArgumentParser()
    parser.add_argument("--dbpattern", type=dbpattern_regex_type, default='%', help="database pattern to limit search")
    parser.add_argument("--skip-dbs", type=csv_list, default=[], help="databases to ignore (comma separated)")
    parser.add_argument("--limit", type=int, default=0, help="Limit number of databases (Default: 0 / unlimited)")
    parser.add_argument("--not-before", type=str, help="Limit tables by age (update_time)")
    parser.add_argument("--login-path", type=str, default='client', help="login_path for authentication (Default: client)")
    parser.add_argument('--backup-dir', default='backups', help='Path for mysqldump backups (Default: backups)')
    parser.add_argument("--dryrun", "--dry-run", action='store_true', help="Don't convert anything, just output MyISAM tables")
    parser.add_argument('--logdir', default='logs', help='Logdir path (Default: logs)')
    parser.add_argument('--verbose', action='store_true', help='Also print all log entries')
    parser.add_argument('--debug', action='store_true', help='Enable debug mode')
    args = parser.parse_args()

    # Configure logging
    logger = configure_logging(args.logdir, args.verbose, args.debug)

    # Always log full command with all arguments on first logline
    logger.info(path.basename(__file__) + ' ' + ' '.join(sys.argv[1:]))

    migrator = Migrator(logger, args.login_path, args.backup_dir, args.dryrun, args.not_before)
    migrator.convert_all(args.dbpattern, args.skip_dbs, args.limit)

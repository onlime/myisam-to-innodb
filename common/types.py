import argparse
import re

def dbpattern_regex_type(value: str, pattern: object=re.compile(r"^[a-zA-Z0-9-_%]{1,32}$")):
    if not pattern.match(value):
        raise argparse.ArgumentTypeError
    return value

def csv_list(value: str):
    value = ''.join(value.split()) # removes all whitespace
    return value.split(',')

#!/usr/bin/env python3
import myloginpath
from MySQLdb import connect

class MySQL(object):
    # connect via login path (stored in ~/.mylogin.cnf)
    def __init__(self, login_path: str = 'client', not_before: str = None):
        self.login_path = login_path
        dbconf = myloginpath.parse(login_path)
        self.db = connect(**dbconf)
        self.cur = self.db.cursor()
        self.legacy_engine = 'MyISAM'
        self.not_before_time = not_before or '1970-01-01'

    def get_dbs_with_myisam_tbls(self, dbpattern: str = None):
        self.cur.execute(
            """SELECT table_schema db
            FROM information_schema.TABLES
              WHERE engine = %s AND table_type = 'BASE TABLE'
                AND table_schema NOT IN ('information_schema', 'sys', 'performance_schema', 'mysql')
                AND table_schema LIKE %s
                AND (update_time IS NULL OR update_time > %s)
                  GROUP BY table_schema ORDER BY table_schema""",
            (self.legacy_engine, dbpattern or '%', self.not_before_time)
        )
        return self.cur.fetchall()
    
    def get_all_myisam_tables(self, dbpattern: str = None):
        self.cur.execute(
            """SELECT table_schema db, table_name tbl, row_format, CEIL((IFNULL(data_length, 0) + IFNULL(index_length, 0))/1024) size_kb 
            FROM information_schema.TABLES
              WHERE engine = %s AND table_type = 'BASE TABLE'
                AND table_schema NOT IN ('information_schema', 'sys', 'performance_schema', 'mysql')
                AND table_schema LIKE %s
                AND (update_time IS NULL OR update_time > %s)
                  ORDER BY table_schema, table_name""",
            (self.legacy_engine, dbpattern or '%', self.not_before_time)
        )
        return self.cur.fetchall()
    
    def get_myisam_tables(self, dbname: str = None):
        self.cur.execute(
            """SELECT table_name tbl, row_format, CEIL((IFNULL(data_length, 0) + IFNULL(index_length, 0))/1024) size_kb 
            FROM information_schema.TABLES
              WHERE engine = %s AND table_type = 'BASE TABLE'
                AND table_schema NOT IN ('information_schema', 'sys', 'performance_schema', 'mysql')
                AND table_schema = %s
                AND (update_time IS NULL OR update_time > %s)
                  ORDER BY table_name""",
            (self.legacy_engine, dbname, self.not_before_time)
        )
        return self.cur.fetchall()

    def execute(self, query: str):
        return self.cur.execute(query)

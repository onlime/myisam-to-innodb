# MyISAM-to-InnoDB Converter

## Requirements

- MySQL 8.0 (may also work for MySQL 5.7) / Percona Server for MySQL
- `mysql_config_editor` tool for MySQL login-path
- Python >= 3.5 (only tested on Python 3.9)

## Prerequisites

We need [`mysql_config_editor`](https://dev.mysql.com/doc/refman/8.0/en/mysql-config-editor.html) to store authentication credentials in an obfuscated login path file named `.mylogin.cnf`.

Install `mysql_config_editor`:

```bash
# Percona Server for MySQL 8.0 / Debian Linux
$ apt install libperconaserverclient21-dev

# Homebrew / macOS
$ brew install mysql
```

Create login-path for MySQL `root` (will create `~/.mylogin.cnf`) to allow passwordless logins:

```bash
$ mysql_config_editor set --user=root --password
Enter password;

# print all login-pathes (passwords masked as *****)
$ mysql_config_editor print --all
```

Test if you can login as root without providing credentials:

```bash
$ mysql
# or by providing explicit login-path (client is the default login-path)
$ mysql --login-path=client
```

## Install

Install this project using Python 3 venv:

```bash
$ git clone git@gitlab.com:onlime/myisam-to-innodb.git
$ cd myisam-to-innodbd
$ python3 -m venv venv
$ . venv/bin/activate
(venv)$ pip install --upgrade pip
(venv)$ pip install -r requirements.txt
```

## Usage

### Command help

MyISAM-to-InnoDB `convert.py` is pretty powerful, check `convert.py --help` for help:

```
usage: convert.py [-h] [--dbpattern DBPATTERN] [--skip-dbs SKIP_DBS] [--limit LIMIT] [--not-before NOT_BEFORE]
                  [--login-path LOGIN_PATH] [--backup-dir BACKUP_DIR] [--dryrun] [--logdir LOGDIR] [--verbose] [--debug]

optional arguments:
  -h, --help            show this help message and exit
  --dbpattern DBPATTERN
                        database pattern to limit search
  --skip-dbs SKIP_DBS   databases to ignore (comma separated)
  --limit LIMIT         Limit number of databases (Default: 0 / unlimited)
  --not-before NOT_BEFORE
                        Limit tables by age (update_time)
  --login-path LOGIN_PATH
                        login_path for authentication (Default: client)
  --backup-dir BACKUP_DIR
                        Path for mysqldump backups (Default: backups)
  --dryrun, --dry-run   Don't convert anything, just output MyISAM tables
  --logdir LOGDIR       Logdir path (Default: logs)
  --verbose             Also print all log entries
  --debug               Enable debug mode
```

### Examples

Usage examples:

> **WARNING:** Do not try this at home! We have added `--dryrun` to every command, so that it won't change anything on your existing data. Once you are sure what the command does, run it without the `--dryrun` flag.

```bash
$ cd <PROJECT_DIR>
$ . venv/bin/activate

# Convert all user tables
(venv)$ ./convert.py --dryrun

# Convert all tables of a single database
(venv)$ ./convert.py --dbpattern=my_db1 --dryrun

# Convert all tables of a single database
(venv)$ ./convert.py --dbpattern=dbname --dryrun

# Convert all tables of all databases by pattern
(venv)$ ./convert.py --dbpattern='my_db%' --dryrun

# Convert all tables of all databases by pattern and write backups to some external dir
(venv)$ ./convert.py --dbpattern='my_db%' --backup-dir=/backups/myisam-dumps --dryrun

# Convert all tables of all databases but exclude some single dbs (comma separated list)
(venv)$ ./convert.py --skip-dbs='my_db1,my_db2' --dryrun
# or even combine such excludes with a db pattern
(venv)$ ./convert.py --dbpattern='my_db%' --skip-dbs='my_db1,my_db2' --dryrun

# Convert all tables and output loglines (in addition to application.log)
(venv)$ ./convert.py --dryrun --verbose

# Convert all tables, limited by the next 10 databases with MyISAM tables
(venv)$ ./convert.py --limit=10 --dryrun --verbose

# Convert all tables, limited by the next 10 databases with MyISAM tables, 
# also reporting debug messages (e.g. creation of single table backup dumps) on STDOUT
(venv)$ ./convert.py --limit=10 --dryrun --verbose --debug

# Finally, you can limit MyISAM table lookup by age. As we cannot count on create_time, we
# only limit by update_time from information_schema.TABLES
# This is only used for a very special use case if you want to exclude really old MyISAM 
# tables which might have been created/used prior a MySQL 5.7 upgrade, avoiding problems like:
# https://bugs.mysql.com/bug.php?id=99791
(venv)$ ./convert.py --not-before='2018-01-01' --dryrun --verbose
```

Or run it in a one-liner (without entering the venv):

```bash
$ cd <PROJECT_DIR> && venv/bin/python convert.py --dryrun
```

If you did not specify `--verbose`, you might want to monitor the log:

```bash
$ tail -f logs/application.log
```

### Cronjob

If running this in a cronjob, ensure the following:

- The user needs to has access to the right `--login-path` which is stored in his `~/.mylogin.cnf`. Usually this will be `root`, as we don't want to store MySQL root credentials in any regular system user.
-  The `convert.py` script needs to run inside your project's venv

example of a nightly cronjob at 04:15AM:

```bash
15 4	* * *	root	cd PROJECT_DIR && venv/bin/python convert.py --limit=10 --backup-dir=/backups/myisam-dumps
```

## Manual Queries

### List all MyISAM tables

This is the query, myisam-to-innodb basically does to list all `MyISAM` tables (+ table size) of all user databases:

```sql
SELECT table_schema db, table_name tbl, CAST((IFNULL(data_length, 0) + IFNULL(index_length, 0)) AS SIGNED) size 
    FROM information_schema.TABLES
        WHERE engine = 'MyISAM' AND table_type = 'BASE TABLE'
            AND table_schema NOT IN ('information_schema', 'sys', 'performance_schema', 'mysql')
            -- AND table_schema LIKE 'your_database'
                ORDER BY table_schema, table_name;
```

Only list databases with at least one MyISAM table:

```sql
SELECT table_schema db FROM information_schema.TABLES
    WHERE engine = 'MyISAM' AND table_type = 'BASE TABLE'
        AND table_schema NOT IN ('information_schema', 'sys', 'performance_schema', 'mysql')
            GROUP BY table_schema ORDER BY table_schema;
```

Or do it all with a nice on-liner for plain output (batch mode), one db name per line:

```bash
# full list of all tables
$ mysql -sN -r -e "SELECT CONCAT(table_schema, '.', table_name) FROM information_schema.TABLES 
    WHERE engine = 'MyISAM' AND table_type = 'BASE TABLE'
        AND table_schema NOT IN ('information_schema', 'sys', 'performance_schema', 'mysql')
            ORDER BY table_schema, table_name"

# only list databases
$ mysql -sN -r -e "SELECT table_schema db FROM information_schema.TABLES
    WHERE engine = 'MyISAM' AND table_type = 'BASE TABLE'
        AND table_schema NOT IN ('information_schema', 'sys', 'performance_schema', 'mysql')
            GROUP BY table_schema ORDER BY table_schema"
```

### Convert single table to InnoDB

To convert a single table to InnoDB, run:

```sql
ALTER TABLE `db`.`$tbl` ENGINE=InnoDB;
```

### Full database backup dump

That's how myisam-to-innodb backups databases prior to changing table engines to `InnoDB`:

```bash
$ mysqldump --routines --events --single-transaction --databases your_database | gzip > your_database.sql.gz
```

You could then also convert all tables from `MyISAM` to `InnoDB` by directly loading the dump like this:

```bash
$ DBNAME=your_database
$ zcat $DBNAME.sql.gz | sed 's/ENGINE=MyISAM/ENGINE=InnoDB/g' | sed 's/ROW_FORMAT=FIXED//g' | mysql $DBNAME
```

This is a possible alternative to `ALTER TABLE ... ENGINE=InnoDB`, but would result in some downtime.
But beware, this is a full text search replacement, so it would also mess with data. Definitely not recommended
if you run this on some tech blogger's database who posts any MySQL CREATE TABLE statements!
So rather use this project's convert.py

## TODO

- [ ] Document "Row size too large (> 8126)" problem on MyISAM tables with large amount of columns

## About myisam-to-innodb

The project is hosted on [GitLab](https://gitlab.com/onlime/myisam-to-innodb).
Please use the project's [issue tracker](https://gitlab.com/onlime/myisam-to-innodb/issues) if you find bugs.

myisam-to-innodb was written by [Philip Iezzi](https://gitlab.com/piezzi) for [Onlime GmbH](https://www.onlime.ch).
